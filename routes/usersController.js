const bcrypt = require('bcrypt');
const jwtUtils = require('../utils/jwt.utils');
const db = require('../models');
const asyncLib = require('async');
const sequelize = require('sequelize');
const nodemailer = require("nodemailer");
const ejs = require('ejs');

const EMAIL_REGEX = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
const PASSWORD_REGEX = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$/;

// Routes
module.exports = ((app) => {

    //////////////////////////////
    // Inscription utilisateur ///
    //////////////////////////////

    app.post('/users/register/', (req, res) => {
        // Récupérer les paramètres
        const email = req.body.email;
        const password = req.body.password;
        const pseudo = req.body.pseudo;

        if (email == null || password == null || pseudo == null) {
            return res.status(400).json({'erreur': 'Paramètre(s) manquant(s)'})
        }

        if (pseudo.length >= 15 || pseudo.length <= 4) {
            return res.status(400).json({'erreur': 'Le pseudo doit être entre 5 et 14 caractères'})
        }

        if (!EMAIL_REGEX.test(email)) {
            return res.status(400).json({'erreur': 'L\'email n\'est pas valide'});
        }

        if (!PASSWORD_REGEX.test(password)) {
            return res.status(400).json({'erreur': 'Le mot de passe doit contenir au moins 6 caractères, 1 lettre et 1 chiffre'});
        }

        asyncLib.waterfall([
            (done) => {
                // Verifie si l'email OU le pseudo existe déjà
                db.User.findOne({
                    where: {
                        [sequelize.Op.or]: [
                            {email: req.body.email}, {pseudo: req.body.pseudo}
                        ]
                    }
                }).then((userFound) => {
                    done(null, userFound);
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Ne peut pas vérifier si l\'utilisateur existe'});
                });
            },
            (userFound, done) => {
                if (!userFound) {
                    bcrypt.hash(password, 5, (err, bcryptedPassword) => {
                        done(null, userFound, bcryptedPassword);
                    });
                } else {
                    return res.status(409).json({'erreur': 'Email ou pseudo existe déjà'});
                }
            },
            (userFound, bcryptedPassword, done) => {
                const newUser = db.User.create({
                    email: email,
                    pseudo: pseudo,
                    password: bcryptedPassword
                }).then((newUser) => {
                    done(newUser);
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Ne peut pas inscrire l\'utilisateur'});
                });
            }
        ], (newUser) => {
            if (newUser) {
                return res.status(201).json({
                    'userId': newUser.id
                });
            } else {
                return res.status(500).json({'erreur': 'Ne peut pas inscrire l\'utilisateur'});
            }
        });
    });

    /////////////////////////////////////////
    // Inscription/Connexion avec Facebook //
    ////////////////////////////////////////

    app.post('/users/register_facebook/', (req, res) => {
        // Récupérer les paramètres
        const email = req.body.email;
        const pseudo = req.body.pseudo;
        const idFacebook = req.body.idFacebook;
        const img = req.body.img;

        if (email == null || pseudo == null || idFacebook == null) {
            return res.status(400).json({'erreur': 'Paramètre(s) manquant(s)'})
        }

        asyncLib.waterfall([
            (done) => {
                // Verifie si l'email OU le pseudo existe déjà
                db.User.findOne({
                    where: {
                        [sequelize.Op.or]: [
                            {email: req.body.email}, {pseudo: req.body.pseudo}, {idFacebook: req.body.idFacebook}
                        ]
                    }
                }).then((userFound) => {
                    done(null, userFound);
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Ne peut pas vérifier si l\'utilisateur existe'});
                });
            },
            (userFound, done) => {
                if (!userFound) {
                    const newUser = db.User.create({
                        email: email,
                        pseudo: pseudo,
                        idFacebook: idFacebook,
                        img: img
                    }).then((newUser) => {
                        done(newUser);
                    }).catch((err) => {
                        return res.status(500).json({'erreur': 'Ne peut pas inscrire l\'utilisateur'});
                    });
                } else {
                    return res.status(201).json({
                        'pseudo': userFound.pseudo,
                        'email': userFound.email,
                        'token': jwtUtils.generateToken(userFound),
                        'img': userFound.img
                    });
                }
            }
        ], (newUser) => {
            if (newUser) {
                return res.status(201).json({
                    'pseudo': newUser.pseudo,
                    'email': newUser.email,
                    'token': jwtUtils.generateToken(newUser),
                    'img': newUser.img
                });
            } else {
                return res.status(500).json({'erreur': 'Ne peut pas inscrire l\'utilisateur'});
            }
        });
    });

    ////////////////////////////
    // Connexion utilisateur ///
    ////////////////////////////

    app.post('/users/login/', (req, res) => {
        // Récupérer les paramètres
        const email = req.body.email;
        const password = req.body.password;

        if (email == null || password == null) {
            return res.status(400).json({'erreur': 'paramètre(s) manquant(s)'})
        }

        asyncLib.waterfall([
            (done) => {
                // Vérifie l'email de l'utilisateur
                db.User.findOne({
                    where: {email: req.body.email}
                }).then((userFound) => {
                    done(null, userFound);
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible de vérifier l\'utilisateur'});
                });
            },
            (userFound, done) => {
                if (userFound) {
                    // Compare le mot de passe avec celui de la bdd
                    bcrypt.compare(password, userFound.password, (errBcrypt, resBcrypt) => {
                        // test le mot de passe
                        done(null, userFound, resBcrypt);
                    });
                } else {
                    return res.status(404).json({'erreur': 'L\'utilisateur n\'existe pas'});
                }
            },
            (userFound, resBcrypt, done) => {
                if (resBcrypt) {
                    done(userFound);
                } else {
                    // Si le mot de passe est incorrect
                    return res.status(403).json({'erreur': 'Mot de passe incorrect'});
                }
            }
        ], (userFound) => {
            if (userFound) {
                // Si le mot de passe correspond bien
                return res.status(200).json({
                    'pseudo': userFound.pseudo,
                    'email': userFound.email,
                    'token': jwtUtils.generateToken(userFound)
                });
            } else {
                return res.status(500).json({'erreur': 'Impossible de vérifier l\'utilisateur'});
            }
        });
    });

    //////////////////////////////////////////////////
    // Récupérer les informations de l'utilisateur //
    /////////////////////////////////////////////////

    app.get('/users/profile/', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        if (userId < 0) {
            return res.status(400).json({'erreur': 'Mauvais token'});
        }

        db.User.findOne({
            attributes: ['email', 'pseudo'],
            where: {'id': userId}
        }).then((user) => {
            if (user) {
                res.status(201).json({user})
            } else {
                res.status(404).json({'erreur': 'L\'utilisateur n\'a pas été trouvé'});
            }
        }).catch((err) => {
            res.status(500).json({'erreur': 'Ne peut pas chercher l\'utilisateur'});
        });
    });

    //////////////////////////////////////////////////
    // Modifier les informations de l'utilisateur ///
    /////////////////////////////////////////////////

    app.put('/users/profile/', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        // Récupérer les paramètres
        const email = req.body.email;
        const pseudo = req.body.pseudo;

        if (email == null || pseudo == null) {
            return res.status(400).json({'erreur': 'Paramètre(s) manquant(s)'})
        }

        if (pseudo.length >= 15 || pseudo.length <= 4) {
            return res.status(400).json({'erreur': 'Le pseudo doit être entre 5 et 14 caractères'})
        }

        if (!EMAIL_REGEX.test(email)) {
            return res.status(400).json({'erreur': 'L\'email n\'est pas valide'});
        }

        asyncLib.waterfall([
            (done) => {
                db.User.findOne({
                    attributes: ['id', 'email', 'pseudo'],
                    where: {id: userId}
                }).then((userFound) => {
                    done(null, userFound);
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible de vérifier l\'utilisateur'});
                });
            },
            (userFound, done) => {
                if (userFound) {
                    db.User.update(
                        {email: email, pseudo: pseudo}, {
                            where: {
                                email: userFound.email,
                                pseudo: userFound.pseudo
                            }
                        }).then(() => {
                        done(userFound);
                    }).catch((err) => {
                        res.status(500).json({'erreur': 'Impossible de modifier l\'utilisateur'});
                    });
                } else {
                    res.status(404).json({'erreur': 'Utilisateur pas trouvé'});
                }
            }
        ], (userFound) => {
            if (userFound) {
                return res.status(201).json(userFound);
            } else {
                return res.status(500).json({'erreur': 'Impossible de modifier l\'utilisateur'});
            }
        });
    });

    //////////////////////////
    // Mot de passe oublié ///
    //////////////////////////

    app.post('/users/forgotten_password', async (req, res) => {
        // Récupérer les paramètres
        const userEmail = req.body.email;

        const transporter = nodemailer.createTransport({
            host: "localhost",
            port: 1025,
            ignoreTLS: true
        });

        asyncLib.waterfall([
            (done) => {
                db.User.findOne({
                    where: {'email': userEmail}
                }).then((userFound) => {
                    done(null, userFound)
                }).catch((err) => {

                });
            }, (userFound, done) => {
                if (!userFound) {
                    res.status(404).json({'erreur': 'L\'email n\'existe pas'})
                } else {
                    done(null, userFound);
                }
            }, (userFound, done) => {
                const token = Math.random().toString(36).substr(2);
                const token2 = Math.random().toString(36).substr(2);
                const token3 = token + token2;
                if (userFound) {
                    userFound.update(
                        {resetToken: token3}, {
                            where: {
                                resetToken: userFound.resetToken
                            }
                        }).then(() => {
                        done(userFound);
                    }).catch((err) => {
                        res.status(500).json({'erreur': 'Impossible de mettre à jour le token de réinitialisation de mot de passe'});
                    });
                } else {
                    res.status(404).json({'erreur': 'Utilisateur pas trouvé'});
                }
            }
        ], (userFound) => {
            ejs.renderFile('./templates/resetPassword/html.ejs', {token: userFound.resetToken}, (err, str) => {
                if (err) {
                    return res.status(500).json({'erreur': 'Impossible de récuper le contenu de l\'email'});
                } else {
                    const mainOptions = {
                        from: '"Bibliotech" contact@bibliotech.fr',
                        to: userEmail,
                        subject: 'Mot de passe oublié',
                        html: str
                    };
                    transporter.sendMail(mainOptions, (err, info) => {
                        if (err) {
                            return res.status(500).json({'erreur': 'Email non-envoyé'});
                        } else {
                            return res.status(200).json({'message': 'Email envoyé'});
                        }
                    });
                }
            });
        });
    });

    app.get('/users/reset_password/:token', (req, res) => {
        const token = req.params.token;
        res.render('../templates/resetPassword/reset', {token})
    });

    app.post('/users/reset_password_submit/:token', (req, res) => {
        // Récupérer les paramètres
        const password = req.body.password;
        const token = req.params.token;

        if (!PASSWORD_REGEX.test(password)) {
            return res.status(400).json({'erreur': 'Le mot de passe doit contenir au moins 6 caractères, 1 lettre et 1 chiffre'});
        }

        asyncLib.waterfall([
            (done) => {
                db.User.findOne({
                    where: {resetToken: token}
                }).then((userFound) => {
                    done(null, userFound)
                }).catch((err) => {
                    res.status(404).json({'erreur': 'L\'utilisateur n\'a pas été trouvé'})
                })
            }, (userFound, done) => {
                if (userFound) {
                    bcrypt.hash(password, 5, (err, bcryptedPassword) => {
                        done(userFound, bcryptedPassword);
                    });
                } else {
                    return res.status(409).json({'erreur': 'Email ou pseudo existe déjà'});
                }
            }
        ], (userFound, bcryptedPassword) => {
            if (userFound) {
                userFound.update({
                    resetToken: null,
                    password: bcryptedPassword
                }).then(() => {
                    return res.status(200).json({'message': 'Mot de passe mis à jour'});
                }).catch((err) => {
                    res.status(500).json({'erreur': 'Impossible de mettre à jour le mot de passe'});
                });
            } else {
                res.status(404).json({'erreur': 'Utilisateur pas trouvé'});
            }
        });
    });

    //////////////////////////////////
    // Ajout d'un livre en favoris ///
    //////////////////////////////////

    app.post('/users/add_book/', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        const idBook = req.body.idBook;

        asyncLib.waterfall([
            (done) => {
                db.Library.findOne({
                    where: {UserId: userId, idBook: idBook}
                }).then((book) => {
                    if (!book) {
                        done(null)
                    } else {
                        return res.status(403).json({'erreur': 'Livre déjà en favoris'});
                    }
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible de vérifier le livre'});
                });
            }, (done) => {
                db.Library.create({
                    idBook: idBook, UserId: userId, StatusId: 3
                }).then((success) => {
                    done(success)
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible d\'ajouter le livre'});
                })
            }
        ], (success) => {
            if (success) {
                return res.status(200).json({'message': 'Livre ajouté avec succès'});
            }
        });
    });

    //////////////////////////////////////
    // Affichage des livres en favoris ///
    //////////////////////////////////////

    app.get('/users/books/', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        let books = [];

        asyncLib.waterfall([
            (done) => {
                db.User.findOne({
                    where: {id: userId}
                }).then((userFound) => {
                    done(null, userFound);
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible de trouver l\'utilisateur'});
                });
            }, (userFound, done) => {
                db.Library.findAll({
                    where: {UserId: userId},
                    order: [
                        ['createdAt', 'DESC']
                    ]
                }).then((successBooks) => {
                    done(successBooks);
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible d\'afficher les livres'});
                });
            }
        ], (successBooks) => {
            if (successBooks) {
                for (let i = 0; i < successBooks.length; i++) {
                    books.push('https://www.googleapis.com/books/v1/volumes/' + successBooks[i].idBook);
                }
                return res.status(200).json({'books': books, 'resFromDB': successBooks});
            } else {
                return res.status(500).json({'erreur': 'Impossible de réaliser se traitement'});
            }
        });
    });

    ///////////////////////////////////
    // Ajout d'un livre en wishlist ///
    ///////////////////////////////////

    app.post('/users/add_wish/', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        const idBook = req.body.idBook;

        console.log(idBook);
        console.log(userId);

        asyncLib.waterfall([
            (done) => {
                db.Wish.findOne({
                    where: {UserId: userId, idBook: idBook}
                }).then((book) => {
                    if (!book) {
                        done(null)
                    } else {
                        return res.status(403).json({'erreur': 'Livre déjà en wishlist'});
                    }
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible de vérifier le livre'});
                });
            }, (done) => {
                db.Wish.create({
                    idBook: idBook, UserId: userId
                }).then((success) => {
                    done(success)
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible d\'ajouter le livre'});
                })
            }
        ], (success) => {
            if (success) {
                return res.status(200).json({'message': 'Livre ajouté avec succès'});
            }
        });
    });

    //////////////////////////////////////
    // Affichage des livres en wishlist ///
    //////////////////////////////////////

    app.get('/users/wish', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        let books = [];

        asyncLib.waterfall([
            (done) => {
                db.User.findOne({
                    where: {id: userId}
                }).then((userFound) => {
                    done(null, userFound);
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible de trouver l\'utilisateur'});
                });
            }, (userFound, done) => {
                db.Wish.findAll({
                    where: {UserId: userId}
                }).then((successBooks) => {
                    done(successBooks);
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible d\'afficher les livres'});
                });
            }
        ], (successBooks) => {
            if (successBooks) {
                for (let i = 0; i < successBooks.length; i++) {
                    books.push('https://www.googleapis.com/books/v1/volumes/' + successBooks[i].idBook);
                }
                return res.status(200).json({'books': books});
            } else {
                return res.status(500).json({'erreur': 'Impossible de réaliser se traitement'});
            }
        });
    });

    //////////////////////////////////////////////
    // Récupérer un id de livre (Bibliothèque) ///
    //////////////////////////////////////////////

    app.get('/users/book/library/:id', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        // Récupération de l'id du livre
        const idBook = req.params.id;
        db.Library.findOne({
            where: {UserId: userId, idBook: idBook}
        }).then((book) => {
            if (!book) {
                return res.json(0);
            } else {
                return res.json(1);
            }
        }).catch((err) => {

        });
    });

    //////////////////////////////////////////////
    // Récupérer un id de livre (Wishlist) ///////
    //////////////////////////////////////////////

    app.get('/users/book/wish/:id', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        // Récupération de l'id du livre
        const idBook = req.params.id;
        db.Wish.findOne({
            where: {UserId: userId, idBook: idBook}
        }).then((book) => {
            if (!book) {
                return res.json(0);
            } else {
                return res.json(1);
            }
        }).catch((err) => {

        });
    });

    /////////////////////////////////////////
    // Supprimer un livre (Bibliothèque) ///
    ///////////////////////////////////////

    app.delete('/users/book/library/delete/:id', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        // Récupération de l'id du livre
        const idBook = req.params.id;

        asyncLib.waterfall([
            (done) => {
                db.Library.findOne({
                    where: {
                        idBook: idBook, UserId: userId
                    }
                }).then((bookFound) => {
                    done(null, bookFound)
                }).catch((err) => {
                    res.status(400).json({'erreur': 'Livre non trouvé, ne peut-être supprimé'})
                });
            }, (bookFound, done) => {
                db.Bookmark.destroy({
                    where: {
                        LibraryId: bookFound.id, UserId: userId
                    }
                }).then((success) => {
                    done(null, success)
                }).catch((err) => {
                    res.status(500).json({'erreur': 'Impossible de supprimer le marque-page'})
                });
            }, (success, done) => {
                db.Library.destroy({
                    where: {
                        UserId: userId, idBook: idBook
                    }
                }).then((destroyed) => {
                    if (destroyed) {
                        done(destroyed)
                    }
                }).catch((err) => {
                    res.status(500).json({'erreur': 'Impossible de supprimer le livre'});
                })
            }
        ], (destroyed) => {
            if (destroyed) {
                res.status(203).json({'message': 'Livre supprimé avec succès'});
            } else {
                res.status(500).json({'erreur': 'Impossible de supprimer le livre'});
            }
        });
    });

    /////////////////////////////////////
    // Supprimer un livre (Wishlist) ///
    ////////////////////////////////////

    app.delete('/users/book/wish/delete/:id', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        // Récupération de l'id du livre
        const idBook = req.params.id;

        db.Wish.destroy({
            where: {
                idBook: idBook, UserId: userId
            }
        }).then((book) => {
            if (book) {
                return res.status(200).json({'message': 'Livre supprimer de votre wishlist'});
            }
        }).catch((err) => {
            return res.status(500).json({'erreur': 'Impossible de supprimer le livre de votre wishlist'});
        });
    });

    /////////////////////////////////
    // Récuperer tout les statuts ///
    ////////////////////////////////

    app.get('/statuses', (req, res) => {
        db.Status.findAll().then((statuses) => {
            return res.status(200).json(statuses);
        }).catch((err) => {
            return res.status(500).json({'erreur': 'Impossible de récupérer les statuts'});
        });
    });

    /////////////////////////////////////
    // Récuperer le statut d'un livre ///
    ////////////////////////////////////

    app.get('/users/book/status/:id', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        // Récupération de l'id du livre
        const idBook = req.params.id;

        db.Library.findOne({
            where: {
                idBook: idBook, UserId: userId
            }
        }).then((book) => {
            if (book) {
                return res.status(200).json(book);
            }
        }).catch((err) => {
            return res.status(500).json({'erreur': 'Impossible de récupérer le statut du livre'});
        });
    });

    ///////////////////////////////////
    // Changer le statut d'un livre ///
    //////////////////////////////////

    app.put('/users/book/status/', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        // Récupération de l'id du statut
        const status = req.body.status;
        const idBook = req.body.idBook;

        asyncLib.waterfall([
            (done) => {
                db.Library.findOne({
                    where: {
                        idBook: idBook, UserId: userId
                    }
                }).then((book) => {
                    done(null, book)
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible de récupérer le livre'});
                });
            }, (book, done) => {
                if (book) {
                    db.Library.update(
                        {StatusId: status}, {
                            where: {
                                idBook: idBook, UserId: userId
                            }
                        }).then(() => {
                        done(book);
                    }).catch((err) => {
                        res.status(500).json({'erreur': 'Impossible de modifier le statut du livre'});
                    });
                } else {
                    res.status(404).json({'erreur': 'Impossible de récupérer le livre'});
                }
            }
        ], (book) => {
            if (book) {
                res.status(201).json({'message': 'Statut du livre modifié'});
            } else {
                res.status(500).json({'erreur': 'Impossible de modifier le statut du livre'});
            }
        });
    });

    ////////////////////////////
    // Mettre un marque page ///
    ////////////////////////////

    app.post('/users/bookmark', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        const idBook = req.body.idBook;

        console.log(idBook);
        console.log(userId);

        asyncLib.waterfall([
            (done) => {
                db.Library.findOne({
                    where: {
                        idBook: idBook, UserId: userId
                    }
                }).then((book) => {
                    done(null, book)
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible de récupérer le livre'});
                });
            }, (book, done) => {
                db.Bookmark.findOne({
                    where: {
                        UserId: userId,
                        LibraryId: book.id
                    }
                }).then((data) => {
                    if (!data) {
                        done(null, book)
                    } else {
                        return res.status(500).json({'erreur': 'Il ya déjà un marque page'});
                    }
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Il ya déjà un marque page'});
                });
            }, (book, done) => {
                db.Bookmark.create({
                    page: 0,
                    UserId: userId,
                    LibraryId: book.id
                }).then((res) => {
                    done(res)
                }).catch((err) => {
                    return res.status(201).json({'message': 'Marque page crée avec succès', 'data': err})
                })
            }
        ], (res) => {
            return res.status(201).json({'message': 'Marque page crée avec succès', 'data': res})
        });
    });

    //////////////////////////////
    // Modifier le marque page ///
    //////////////////////////////

    app.put('/users/bookmark/', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        const idBook = req.body.idBook;
        const page = req.body.page;

        console.log(idBook);
        console.log(userId);
        console.log(page);

        asyncLib.waterfall([
            (done) => {
                db.Library.findOne({
                    where: {
                        idBook: idBook, UserId: userId
                    }
                }).then((book) => {
                    done(null, book)
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible de récupérer le livre'});
                });
            }, (book, done) => {
                db.Bookmark.findOne({
                    where: {
                        UserId: userId,
                        LibraryId: book.id
                    }
                }).then((data) => {
                    if (data) {
                        done(null, book)
                    } else {
                        return res.status(500).json({'erreur': 'Il n\'y a pas de marque page'});
                    }
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Il ya déjà un marque page'});
                });
            }, (book, done) => {
                db.Bookmark.update(
                    {page: page}, {
                        where: {
                            LibraryId: book.id, UserId: userId
                        }
                    }).then((res) => {
                    done(res)
                }).catch((err) => {
                    return res.status(201).json({'message': 'Marque page modifié avec succès', 'data': err})
                });
            }
        ], (res) => {
            return res.status(201).json({'message': 'Marque page modifié avec succès', 'data': res})
        });
    });

    /////////////////////////////////////////
    // Afficher le marque page d'un livre ///
    ////////////////////////////////////////

    app.get('/users/bookmark/:id', (req, res) => {
        // Récupération des headers
        const headerAuth = req.headers.authorization;
        const userId = jwtUtils.getUserId(headerAuth);

        const idBook = req.params.id;

        console.log(idBook);
        console.log(userId);

        asyncLib.waterfall([
            (done) => {
                db.Library.findOne({
                    where: {
                        idBook: idBook, UserId: userId
                    }
                }).then((book) => {
                    done(book)
                }).catch((err) => {
                    return res.status(500).json({'erreur': 'Impossible de récupérer le livre'});
                });
            }
        ], (book) => {
            db.Bookmark.findOne({
                where: {
                    UserId: userId,
                    LibraryId: book.id
                }
            }).then((data) => {
                return res.status(201).json(data);
            }).catch((err) => {
                return res.status(500).json({'erreur': 'Il ya déjà un marque page'});
            });
        });
    });
});
