'use strict';
module.exports = (sequelize, DataTypes) => {
  const Status = sequelize.define('Status', {
    name: DataTypes.STRING
  }, {});
  Status.associate = function(models) {
    // associations can be defined here
    models.Status.hasMany(models.Library);
  };
  return Status;
};
