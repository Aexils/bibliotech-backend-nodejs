'use strict';
module.exports = (sequelize, DataTypes) => {
  const Wish = sequelize.define('Wish', {
    idBook: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {});
  Wish.associate = function(models) {
    // associations can be defined here
    models.Wish.belongsTo(models.User, {
      foreignKey: {
        allowNull: false
      }
    })
  };
  return Wish;
};
