'use strict';
module.exports = (sequelize, DataTypes) => {
    const Library = sequelize.define('Library', {
        idBook: DataTypes.STRING,
        userId: DataTypes.INTEGER,
        statusId: DataTypes.INTEGER
    }, {});
    Library.associate = function (models) {
        // associations can be defined here
        models.Library.belongsTo(models.User, {
            foreignKey: {
                allowNull: false
            }
        });
        models.Library.belongsTo(models.Status, {
            foreignKey: {
                allowNull: false
            }
        });
        models.Library.hasMany(models.Bookmark);
    };
    return Library;
};
