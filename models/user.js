'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    pseudo: DataTypes.STRING,
    resetToken: DataTypes.STRING,
    idFacebook: DataTypes.STRING,
    img: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
    models.User.hasMany(models.Library);
    models.User.hasMany(models.Wish);
    models.User.hasMany(models.Bookmark);
  };
  return User;
};
