'use strict';
module.exports = (sequelize, DataTypes) => {
  const Bookmark = sequelize.define('Bookmark', {
    page: DataTypes.INTEGER
  }, {});
  Bookmark.associate = function(models) {
    // associations can be defined here
    models.Bookmark.belongsTo(models.Library, {
      foreignKey: {
        allowNull: false
      }
    });
    models.Bookmark.belongsTo(models.User, {
      foreignKey: {
        allowNull: false
      }
    });
  };
  return Bookmark;
};
