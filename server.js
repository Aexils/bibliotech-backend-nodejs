const express = require('express');
const app = express();
const routes = require("./routes/usersController");
const bodyParser = require('body-parser');
const db = require('./models');
const cors = require('cors');


// Activer le CORS pour toutes les routes
app.use(cors());

// Activation du moteur de template
app.set('view engine', 'ejs');

//Configuration de Body Parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Appels de mes routes
app.use('/api', routes);
routes(app);


// Lancer le serveur
db.sequelize.sync().then(() => {
    app.listen(3000, () => {
        console.log("Server started on port 3000");
    });
});

