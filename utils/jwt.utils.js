const jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = 'LuHUBAZGIneWUbzldJjkMgvgElIhOoJbWmCbNdulDnRcSdg6q0Hmb9xElMpIupP1PEXOcbVbp39eCh7T22lJ3cq';

module.exports = {
    generateToken: ((userData) => {
        return jwt.sign({
            'userID': userData.id,
        }, JWT_SIGN_SECRET, {
            expiresIn: '365d'
        });
    }),
    parseAuthorization: ((authorization) => {
        return (authorization != null) ? authorization.replace('Bearer ', '') : null;
    }),
    getUserId: ((authorization) => {
        let userId = -1;
        const token = module.exports.parseAuthorization(authorization);
        if (token != null) {
            try {
                const jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
                if (jwtToken != null) {
                    userId = jwtToken.userID;
                }
            } catch(err) {

            }
            return userId;
        }
    })
};
